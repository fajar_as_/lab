from django import forms

class Formulir(forms.Form):
    nmKegiatan = forms.CharField(max_length=40,widget=forms.TextInput(attrs={'class':'form-control', 'placeholder': 'Enter your Activity e.g. Play Football'}))
    tanggal = forms.DateField(widget=forms.DateInput(attrs={'class':'form-control', 'placeholder':'','type':'date'}))
    tempat = forms.CharField(max_length=40, widget= forms.TextInput(attrs={'class':'form-control', 'placeholder': 'Enter the Location e.g. Arena Futsal'}))
    waktu = forms.TimeField(widget=forms.TimeInput(attrs={'class':'form-control','type':'time'}))
    kategori = forms.CharField(max_length=40,widget=forms.TextInput(attrs={'class':'form-control','placeholder':'Enter the Category e.g. UKOR'}))