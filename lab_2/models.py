from django.db import models

# Create your models here.
class Jadwal(models.Model):
    nmKegiatan = models.CharField(max_length=40)
    tanggal = models.DateField()
    tempat = models.CharField(max_length=40)
    waktu = models.TimeField()
    kategori = models.CharField(max_length=40)
    pilihan = models.BooleanField(default = False)

    def __str__(self):
        return self.nmKegiatan
