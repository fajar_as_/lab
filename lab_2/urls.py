"""myweb URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.urls import path

from .views import hal1
from .views import about
from .views import project
from .views import activity
from .views import contact
from .views import galery
from .views import jadwal
from .views import tambahJadwal
from .views import hapusJadwal
from .views import pilih

app_name = 'lab_2'
# url for app
urlpatterns = [
    path('',hal1, name= 'hal1'),
    path('about/',about, name= 'about'),
    path('project/',project, name= 'project'),
    path('activity/',activity, name= 'activity'),
    path('contact/',contact, name= 'contact'),
    path('galery/',galery, name= 'galery'),
    path('jadwal/',jadwal,name ='jadwal'),
    path('tambahJadwal/',tambahJadwal, name ='tambahJadwal'),
    path('pilih/<jadwal_id>',pilih, name='pilih'),
    path('hapusJadwal/',hapusJadwal,name='hapusJadwal'),

]
