from django.shortcuts import render, redirect
from .forms import Formulir
from .models import Jadwal
from django.views.decorators.http import require_POST

# Create your views here.
def hal1(request):
    return render(request,'story1.html')

def about(request):
    return render(request,'about.html')

def project(request):
    return render(request,'project.html')

def activity(request):
    return render(request,'activity.html')

def contact(request):
    return render(request,'contact.html')

def galery(request):
    return render(request,'galery.html')

def jadwal(request):
    jadwal_list = Jadwal.objects.order_by('tanggal')
    form = Formulir()
    context = {'jadwal_list' : jadwal_list, 'form': form}
    return render(request,'jadwal.html',context)

@require_POST
def tambahJadwal(request):
	form = Formulir(request.POST)
	
	if form.is_valid():
		jadwalBaru = Jadwal(nmKegiatan=request.POST['nmKegiatan'],tanggal=request.POST['tanggal'],tempat=request.POST['tempat'],waktu=request.POST['waktu'],kategori=request.POST['kategori'])
		jadwalBaru.save()

	return redirect('lab_2:jadwal')

def pilih(request, jadwal_id):
	jadwal = Jadwal.objects.get(pk = jadwal_id)

	if jadwal.pilihan == False:
		jadwal.pilihan = True
	else :
		jadwal.pilihan = False
	
	jadwal.save()

	return redirect('lab_2:jadwal')

def hapusJadwal(request):
	Jadwal.objects.filter(pilihan__exact = True).delete()

	return redirect('lab_2:jadwal')

# def tambahJadwal(request):
#     form = Formulir(request.POST)
#     newJadwal = Jadwal(nmKegiatan = request.POST['nmKegiatan'],tanggal = request.POST['tanggal'],tempat=request.POST['tempat'],
#                         waktu = request.POST['waktu'],kategori=request.POST['kategori'])
#     newJadwal.save()
#     return redirect('lab_2:jadwal')

# def hapusJadwal(request, jadwal_id):
    
#     Jadwal.objects.filter(id=jadwal_id).delete()
    
#     return redirect('lab_2:jadwal')
