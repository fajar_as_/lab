const navSlide = () => {
    const slide = document.querySelector('.slide');
    const nav = document.querySelector('.nav-links');
    
   
    slide.addEventListener('click', () => {
        nav.classList.toggle('nav-active');

        slide.classList.toggle('toggle');
    });

}
navSlide();